class ProductsController < ApplicationController
  before_filter :load_product, :only => [:show, :edit, :update, :destroy, :delete]
 
  def index
    @products = Product.all
  end
  
  def new
    @product = Product.new
  end
  
  def create
    @product = Product.new(params[:product])
    if @product.save
      flash[:notice] = "Successfully created product."
      redirect_to @product
    else
      render :action => 'new'
    end
  end
  
  def update
    if @product.update_attributes(params[:product])
      flash[:notice] = "Successfully updated product."
      redirect_to @product
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @product.destroy
    flash[:notice] = "Successfully destroyed product."
    
    respond_to do |format|
      format.html { redirect_to products_url }
      format.js { render :nothing => true }
    end
  end
  
  private
    def load_product
      @product = Product.find(params[:id])
    end
end
